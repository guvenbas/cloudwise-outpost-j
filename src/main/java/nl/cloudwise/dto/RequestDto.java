package nl.cloudwise.dto;

import lombok.Data;

@Data
public class RequestDto {
	String token;
	String ip;
}
