package nl.cloudwise.dto;

import lombok.Value;

import java.util.Map;

@Value
public class LogRequestDto extends RequestDto {
	String email;
	String organization;
	String level;
	String sessionKey;
	String version;
	String message;
	Map<String, String> payload;
}
