package nl.cloudwise.service;

import com.google.cloud.MonitoredResource;
import com.google.cloud.logging.LogEntry;
import com.google.cloud.logging.Logging;
import com.google.cloud.logging.LoggingOptions;
import com.google.cloud.logging.Payload;
import com.google.cloud.logging.Severity;
import nl.cloudwise.dto.LogRequestDto;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class LogService {

	Logging logging = LoggingOptions.getDefaultInstance().getService();

	public void generateLog(LogRequestDto logRequest) {

		LogEntry logEntry = LogEntry.newBuilder(Payload.StringPayload.of(getLogMessage(logRequest)))
				.setSeverity(getLogLevel(logRequest))
				.setLogName("extension")
				.setResource(MonitoredResource.newBuilder("cloud_run_revision").build())
				.setLabels(getLabels(logRequest))
				.build();

		System.out.println("logEntry:" + logEntry);
	}


	private Severity getLogLevel(LogRequestDto logRequest) {
		Severity level;
		switch(logRequest.getLevel()) {
			case "INFO":
				level = Severity.INFO;
				break;
			case "WARN":
				level = Severity.WARNING;
				break;
			default:
				level = Severity.ERROR;
				break;
		}
		return level;
	}

	private HashMap<String, String> getLabels(LogRequestDto logRequest) {
		HashMap<String, String> labels = new HashMap<>();
		labels.put("token", logRequest.getToken());
		labels.put("ip", logRequest.getIp());
		labels.put("email", logRequest.getEmail());
		labels.put("organization", logRequest.getOrganization());
		labels.put("level", logRequest.getLevel());
		labels.put("sessionKey", logRequest.getSessionKey());
		labels.put("version", logRequest.getVersion());
		labels.put("message", logRequest.getMessage());
		labels.putAll(logRequest.getPayload());
		return labels;
	}

	private String getLogMessage(LogRequestDto logRequest) {
		StringBuffer log = new StringBuffer();
		log.append("[" + logRequest.getVersion() + "]");
		log.append("[" + logRequest.getSessionKey() + "]");
		log.append("[" + logRequest.getIp() + "]");
		log.append("[" + logRequest.getOrganization() + "]");
		log.append("[" + logRequest.getEmail() + "]");

		logRequest.getPayload().entrySet().stream().forEach(e -> log.append(":" + e.getValue() + ":"));
		return log.toString();
	}
}
