package nl.cloudwise.controller;

import nl.cloudwise.dto.LogRequestDto;
import nl.cloudwise.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class LogController {

	private LogService logService;

	@Autowired
	LogController(LogService logService) {
		this.logService = logService;
	}


	@PostMapping("/log")
	public void log(@RequestBody LogRequestDto logRequestDto, HttpServletRequest request) {
		//		, @RequestHeader(value="X-FORWARDED-FOR") String ip
		logRequestDto.setIp(request.getRemoteAddr());
		logRequestDto.setToken("");
		logService.generateLog(logRequestDto);
	}
}
